export const state = () => ({
  error: null,
  success: null
})

export const mutations = {
  setError(state, { object, message }) {
    state.error = { object, message }
  },
  setSuccess(state, message) {
    state.success = { object: null, message }
  }
}

export const actions = {
  handleError({ commit }, object) {
    let message
    if (!object.response) {
      message = 'Network error. Please try again in a few minutes'
    } else if (
      object.response.status === 401 &&
      this.$store.state.user.tokenExpiration - this.now / 1000 < 0
    ) {
      message = 'Your session has expired. Please login again.'
      this.$auth.logout()
      this.$router.push({
        name: 'index'
      })
    } else if (object.response && object.response.message) {
      message = object.response.message
    } else if (object.response && object.response.data) {
      message = handleDrfErrors(object.response.data)
    }
    commit('setError', { object, message })
  }
}

const handleDrfErrors = (data) => {
  let m = ''
  if (data.non_field_errors) {
    m = data.non_field_errors.join('. ')
  }
  return m
}
