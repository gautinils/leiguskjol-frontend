export const state = () => ({
  accountTypes: []
})

export const mutations = {
  setAccountTypes(state, payload) {
    state.accountTypes = payload
  }
}

export const actions = {
  async fetchAccountTypes({ commit }) {
    const { data } = await this.$axios.get(`accounting/account-types`)
    commit('setAccountTypes', data)
  }
}
