export const state = () => ({
  clouds: [],
  current: null,
  transactions: [],
  accounts: [],
  customers: [],
  ledgers: [],
  currencies: []
})

export const mutations = {
  addCloud(state, payload) {
    state.clouds.push(payload)
  },
  setList(state, payload) {
    state.clouds = payload
  },
  setCurrent(state, payload) {
    state.current = payload
  },
  addTransaction(state, payload) {
    state.transactions.push(payload)
  },
  setTransactions(state, payload) {
    state.transactions = payload
  },
  setCustomers(state, payload) {
    state.customers = payload
  },
  setLedgers(state, payload) {
    state.ledgers = payload
  },
  setCurrencies(state, payload) {
    state.currencies = payload
  },
  setAccounts(state, payload) {
    state.accounts = payload
  },
  addAccount(state, payload) {
    state.accounts.push(payload)
  }
}

export const actions = {
  async createCloud({ commit }, cloud) {
    const { data } = await this.$axios.post('/fvc/', cloud)
    commit('addCloud', data)
  },
  async fetchClouds({ commit }) {
    const { data } = await this.$axios.get('/fvc/')
    commit('setList', data)
  },
  async fetchTransactions({ commit, state }) {
    const { data } = await this.$axios.get(
      `/fvc/${state.current.id}/transactions/`
    )
    commit('setTransactions', data)
  },
  async createTransaction({ commit, state }, transaction) {
    const { data } = await this.$axios.post(
      `/fvc/${state.current.id}/transactions/`,
      transaction
    )
    commit('addTransaction', data)
  },
  async fetchCustomers({ commit, state }) {
    const { data } = await this.$axios.get(`/fvc/${state.current.id}/customers`)
    commit('setCustomers', data)
  },
  async fetchLedgers({ commit, state }) {
    const { data } = await this.$axios.get(`fvc/${state.current.id}/ledgers`)
    commit('setLedgers', data)
  },
  async fetchCurrencies({ commit }) {
    const { data } = await this.$axios.get(`fvc/currencies`)
    commit('setCurrencies', data)
  },
  async fetchAccounts({ commit, state }) {
    const { data } = await this.$axios.get(`fvc/${state.current.id}/accounts`)
    commit('setAccounts', data)
  },
  async createAccount({ commit, state }, account) {
    const { data } = await this.$axios.post(
      `/fvc/${state.current.id}/accounts/`,
      account
    )
    commit('addAccount', data)
  }
}
