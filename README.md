# leiguskjol-frontend

> Test project for gauti.leiguskjol.is

## Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate

# lint
$ npm run lint

# lint fix
$ npm run lintfix
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).


## Production 
 * Make sure you have [Docker](https://www.docker.com) and [Docker compose](https://docs.docker.com/compose/) installed and the Docker daemon running
 * Run `docker-compose up`
