export default {
  mode: 'universal',

  axios: {
    baseURL:
      process.env.NODE_ENV === 'production'
        ? 'http://52.56.145.141'
        : 'http://localhost:8000'
  },

  router: {
    middleware: ['auth', 'cloud']
  },

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: 'auth/token/', method: 'post', propertyName: 'access' },
          user: { url: 'auth/user/', method: 'get', propertyName: 'user' },
          logout: false
        }
      }
    },
    redirect: {
      home: '/dashboard',
      login: '/'
    }
  },

  /*
   ** Headers of the page
   */
  head: {
    title: 'Igloo Accounting',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/png',
        href:
          'https://leiguskjol-production.s3.amazonaws.com:443/icons/favicon-32x32.png'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Montserrat&display=swap'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['@/assets/css/main.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {
      src: '~/plugins/vuejs-datepicker.js',
      ssr: false
    },
    {
      src: '~/plugins/vuelidate.js'
    }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/style-resources',
    'nuxt-vue-multiselect'
  ],
  /*
   ** Style resources
   */
  styleResources: {
    scss: ['~/assets/css/colors.scss']
  },

  /*
   ** Override default css in main css file
   */
  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false
  },
  /*
   ** Build configuration
   */
  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    }
  }
}
