export default function({ store }) {
  if (!store.state.financialVirtualClouds.current && store.state.auth.user) {
    store.commit('financialVirtualClouds/setCurrent', store.state.auth.user.fvc)
  }
}
